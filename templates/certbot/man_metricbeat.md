# Metrics beat
curl  -x http://10.48.0.147:3128 -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-8.3.0-amd64.deb
sudo dpkg -i metricbeat-8.3.0-amd64.deb


```sh
cat <<'OEF'> /etc/metricbeat/metricbeat.yml
name: metricbeat

metricbeat.config:
  modules:
    path: ${path.config}/modules.d/*.yml
    reload.enabled: false

metricbeat.autodiscover:
  providers:
    - type: docker
      hints.enabled: true

metricbeat.modules:
- module: docker
  metricsets:
    - container
    - cpu
    - diskio
    - healthcheck
    - info
    - memory
    - network
  hosts: [ unix:///var/run/docker.sock ]
  period: 10s
  enabled: true

- module: system
  period: 10s
  metricsets:
    - cpu
    - load
    - memory
    - network
    - process
    - process_summary
    - socket_summary
    #- entropy
    #- core
    #- diskio
    #- socket
    #- service
    #- users
  process.include_top_n:
    by_cpu: 5      # include top 5 processes by CPU
    by_memory: 5   # include top 5 processes by memory
# Configure the mount point of the host’s filesystem for use in monitoring a host from within a container
# hostfs: "/hostfs"

- module: system
  period: 1m
  metricsets:
    - filesystem
    - fsstat
  processors:
  - drop_event.when.regexp:
      system.filesystem.mount_point: '^/(sys|cgroup|proc|dev|etc|host|lib|snap)($|/)'

- module: system
  period: 15m
  metricsets:
    - uptime

monitoring:
  enabled: true
  elasticsearch:
    username: noob
    password: noob


output.elasticsearch:
  hosts: ["https://elastic.mowertii.ru:443"]
  username: "noob"
  password: "noob"
  # If using Elasticsearch's default certificate
  #ssl.ca_trusted_fingerprint: "<es cert fingerprint>"
  period: 5s
  enabled: true

setup.kibana:
  host: "https://kibana.mowertii.ru:443"
  username: "noob"
  password: "noob"
  period: 5s
  enabled: true

OEF


#metricbeat setup

metricbeat modules enable system
systemctl daemon-reload
systemctl restart metricbeat.service

```